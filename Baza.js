window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

         //prefixes of window.IDB objects
         window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
         window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange

         if (!window.indexedDB) {
            window.alert("Your browser doesn't support a stable version of IndexedDB.")
         }

         const forma = [
            { ime: "Nihad", element: "nest1o", nacin: "Text Input", obavezno: "1" },
            { ime: "Emir", element: "nesto", nacin: "Text Input", broj: 2,dodatna: ["Nihad","konj"], obavezno: "2" }
         ];
         var db;
         var request = window.indexedDB.open("baza", 1);

         request.onerror = function(event) {
            console.log("error: ");
         };

         request.onsuccess = function(event) {
            db = request.result;
            console.log("success: "+ db);
         };

         request.onupgradeneeded = function(event) {
            var db = event.target.result;
            var objectStore = db.createObjectStore("formanova", {keyPath: "ime"});

            for (var i in forma) {
               objectStore.add(forma[i]);
            }
         }

         function read(ime) {

            var transaction = db.transaction(["formanova"]);
            var objectStore = transaction.objectStore("formanova");
            var request = objectStore.get(ime);

            request.onerror = function(event) {
               alert("Unable to retrieve daa from database!");
            };

            request.onsuccess = function(event) {
               // Do something with the request.result!
               ;
               if(request.result) {

                  document.getElementById("a").value = request.result.element ;
                  document.getElementById("b").check = request.result.nacin;
                  document.getElementById("c").check = request.result.broj ;
                  document.getElementById("d").value = request.result.obavezno;
               }

               else {
                  alert("Ne postoji takva Forma!");
               }
            };
           //return (result);
         }

         function read1(ime,obavezno1) {
            var transaction = db.transaction(["formanova"]);
            var objectStore = transaction.objectStore("formanova");
            var request = objectStore.get(ime);






            request.onerror = function(event) {
               alert("Unable to retrieve daa from database!");
            };

            request.onsuccess = function(event) {
               // Do something with the request.result!
               ;
               if(request.result && request.result.obavezno.value === obavezno1) {


                  if(request.result.ime){
                  mojaFunckija("polje1");
                  document.getElementById("polje11").value = request.result.ime;
                }

                if(request.result.broj){
                  mojaFunckija("polje3");
                  document.getElementById("polje33").number = request.result.broj ;}
                  if(request.result.obavezno){
                    mojaFunckija("polje4");
                  document.getElementById("polje44").value = request.result.obavezno;}}


               else {
                  alert("Forma ne postoji!");
                  console.log(request.result);
               }
            };
           //return (result);
         }


         function add(ime1,nacin1,obavezno1) {
            var request = db.transaction(["formanova"],"readwrite")
            .objectStore(["formanova"])
            .add({ ime: ime1, nacin: nacin1, obavezno: obavezno1 });

            request.onsuccess = function(event) {
               alert("Dodana je nova forma!");
            };

            request.onerror = function(event) {
               alert("Polja su prazna! ");
            }
         }

         function add(ime1,nacin1,broj1,dodatno1,obavezno1) {
            var request = db.transaction(["formanova"],"readwrite")
            .objectStore(["formanova"])
            .add({ ime: ime1, nacin: nacin1, broj: broj1, dodatno:dodatno1, obavezno: obavezno1 });

            request.onsuccess = function(event) {
               alert("Dodana je nova forma!");
            };

            request.onerror = function(event) {
               alert("Polja nisu popunjena na adekvatan nacin!");
            }
         }
